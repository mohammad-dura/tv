<?php
/**
 * Created by PhpStorm.
 * User: Raphson
 * Date: 4/19/16
 * Time: 18:52
 */
?>
<nav class="side-menu">
    <ul class="side-menu-list">

        <li class="blue">
            <a href="#">
                <i class="font-icon font-icon-dashboard"></i>
                <span class="lbl">Dashboard</span>
            </a>
        </li>

        <li class="purple with-sub">
            <span>
                <i class="font-icon font-icon-editor-video"></i>
                <span class="lbl">Videos</span>
            </span>
            <ul>
                <li><a href="#"><span class="lbl">Add New Video</span></a></li>
                <li><a href="#"><span class="lbl">All Videos</span></a></li>
                <li><a href="#"><span class="lbl">Video Categories</span></a></li>
            </ul>
        </li>
        <li class="purple with-sub">
            <span>
                <i class="font-icon font-icon-case-2"></i>
                <span class="lbl">Subscription</span>
            </span>
            <ul>
                <li><a href="#"><span class="lbl">Add Subscription</span></a></li>
                <li><a href="#"><span class="lbl">Subscription History</span></a></li>
            </ul>
        </li>


    </ul>
</nav>
<!--.side-menu-->
